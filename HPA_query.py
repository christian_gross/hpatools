#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Feb  12 19:40:57 2021

@author: christiangross

This script reads in Ensembl Gene Identifiers and retrieves the HPA RNA tissue specificty and
expression.
"""
import sys
import os
import sqlite3
import argparse
import pandas as pd
import numpy as np

########################################################################
#ArgParse
########################################################################
parser = argparse.ArgumentParser()

parser.add_argument('--HPA_database',
                    type=str,
                    default="/lustre/projects/DTAdb/resources/hpa/HPA_v03.db",
                    help='path to sql database file containing the HPA sqlite database.')
parser.add_argument('--infile',
                    type=str,
                    default="/lustre/projects/DTAdb/resources/hpa/test_query.txt",
                    help='Path to infile containing the query Ensembl Gene Identifiers in one \
                        column without a header.')
parser.add_argument('--outpath',
                    type=str,
                    default="./",
                    help='Path where the output should be stored. The outfile will have the same \
                        file name as the infile with the prefix "HPA-exp_" and "HPA-spec_"')
args = parser.parse_args()

########################################################################
#Body
########################################################################
#read in infile, one ensembl ID per row, generally it is assumed that the file has no header.
infile = pd.read_csv(args.infile,header=None,names=['ensemblID'])

#check if all of the values are proper ensembl gene identifier
try:
    assert ((np.all(infile['ensemblID'].apply(lambda x: x.startswith('ENSG'))))
            and (np.all(infile['ensemblID'].apply(lambda x: len(x)==15))))
except AssertionError:
    sys.exit('At least one value in the column seems not to be an Ensembl Gene Identifier.')


#Here I am connecting to the HPA database
cnx = sqlite3.connect(args.HPA_database)

#specificity query SQL code
specificty_query = """
SELECT DISTINCT 
	Gene_information.Ensembl_ID,
	Gene_information.Name ,
	RNA_specificity.rnaDistribution_description ,
	RNA_specificity.rnaSpecificity_description ,
	RNA_specificity.rnaSpecificity_specificity,
	Tissue_information.tissue_name ,
	Tissue_information.additional_information,
    Assay_information.assayType

FROM 
	Gene_information 

INNER JOIN Gene_Assay_RNA_specificity_linker
	ON Gene_information.Gene_ID = Gene_Assay_RNA_specificity_linker.Gene_ID

INNER JOIN RNA_specificity 
	ON Gene_Assay_RNA_specificity_linker.RNA_Specificity_ID = RNA_specificity.RNA_Specificity_ID 
	
INNER JOIN Assay_information
	ON Assay_information.Assay_ID = Gene_Assay_RNA_specificity_linker.Assay_ID 
	
LEFT JOIN Tissue_information 
	ON RNA_specificity.Tissue_ID = Tissue_information.Tissue_ID 
	
	WHERE Gene_information.Ensembl_ID IN {target_list}
                """.format(target_list='(\''+'\',\''.join(infile.ensemblID.to_list())+'\')')

#expression query SQL code
expression_query = """
SELECT DISTINCT 
	Gene_information.Ensembl_ID,
	Gene_information.Name,
	RNA_Expression.normalizedRNAExpression ,
	RNA_Expression.proteinCodingRNAExpression ,
	RNA_Expression.RNAExpression,
	Tissue_information.tissue_name ,
	Tissue_information.additional_information,
	Assay_information.assayType,
	GTEx_expression.Expression

FROM 
	Gene_information
	
INNER JOIN GTEx_expression
	ON Gene_information.Gene_ID = GTEx_expression.Gene_ID
	
INNER JOIN RNA_Expression
	ON Gene_information.Gene_ID = RNA_Expression.Gene_ID 
	
INNER JOIN Tissue_information 
	ON  RNA_Expression.Tissue_ID = Tissue_information.Tissue_ID 
	
INNER JOIN Assay_information
	ON RNA_Expression.Assay_ID = Assay_information.Assay_ID 
	
	WHERE Gene_information.Ensembl_ID IN {target_list}
                """.format(target_list='(\''+'\',\''.join(infile.ensemblID.to_list())+'\')')

#queries the database and writes results into two files
for query,label in zip([specificty_query,expression_query],['HPA-spec_','HPA-exp_']):
    #Retrieve the data from the sql data set
    dt = pd.read_sql(sql=query, con=cnx)
    #writes the RNA-tissue specificty out to file.
    dt.to_csv(os.path.join(args.outpath,label+os.path.split(args.infile)[1]),sep='\t',index=False)
    
