# HPA-Database

This project contains scripts to generate and query the Human Protein Atlas v.20 as an SQLite database. It supplements the HPA with single cell Type Tissue Cluster data from HPA and gene tissue specific expression from GTEx.

The XML file containing the HPA data can be downloaded here: https://v20.proteinatlas.org/download/proteinatlas.xml.gz

The singleCellType Tissue Cluster data can be downloaded here: https://v20.proteinatlas.org/download/rna_single_cell_type_tissue.tsv.zip 

The GTEx file containing the Gene-Tissue specific expressions can be downloaded here: https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz


The scripts have been tested in python 3.7. To install the script which processes the HPA xml file and generates the SQLlite database requires Python 3.7 or higher. 
Dependencies can be installed via pip with the following command.

```
python -m pip install --upgrade -r requirements.txt
```

This is followed by the installation of the commandline program.

```
python -m pip install -e .
```

Alternatively, a new conda environment can be created from the enclosed .yml file. This environment contains already all required packages.

```
conda env create -f environment.yml
```

Activate the newly created environment and install the script similar to the example above.

```
conda activate hpatools
```

Followed by the installation of the script.

```
python -m pip install -e .
```

This runs the editible install which enables the possibility to update the script without newly installing the script another time. The program can be started from the commandline.
To process the XML file and generate the SQLite database, the XML file needs to be decompressed first. The scRNA file also needs to be decompressed while the GTEx file can either be decompressed or gzipped (.gz). For testing purposes, the repository contains the first ten entries of the proteinatlas.xml file.
The script can be tested using the following command:

```
hpa_create --HPA_database test_db.db --infile first_ten_entries.xml --scRNA_file rna_single_cell_type_tissue.tsv --GTEx_file GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct
```

Alternatively, for the full data run, replace the --infile parameter with the proteinatlas.xml file and give the database the name you prefer the most (--HPA_database parameter). 


v.03 using a single 2 GHz Intel Core i5, it takes around ~16h to generate the full SQLite database.
v.05 speed test for the current version has not been done yet.

HPA_query.py queries the HPA database for RNA expression values and RNA tissue specificity values. To do that the user needs to provide a file containing Ensembl Gene Identifiers in one column without a header (see test_query.txt). It will generate two output files with the prefix 'HPA-exp_' and 'HPA-spec_' containing the gene-tissue expression and gene-tissue specificity data respectively. 
In future, the HPA_query.py script will be further improved by including exclusion criteria and Uniprot accession identifiers as access point.

```
python HPA_query.py --HPA_database HPA_v05.db --infile test_query.txt --outpath ./
```

Description of Tables and Columns:
Gene_information:

	- Gene_ID:	Gene_information table identifier

	- Ensembl_ID	Ensembl Gene Identifier

	- HPA_version	Version of the Human Protein Atlas

	- URL	Url to the HPA website about the gene in question

	- Name	Gene name

	- assembly	Human Assembly version number

	- gencodeVersion	Ensembl gene build number

Assay_information:

	- Assay_ID:	Assay_information table identifier

	- source:	Source information about which institution / organisation / project provided the assay data

	- technology:	Technology applied to generate the associated data

	- assayType:	High level assay type / tissue that was investigated in the respective assay

Tissue_information:

	- Tissue_ID:	Tissue_information table identifier

	- tissue_name:	Name of the tissue and type. Type can be organ, region (of the brain), cellLine, bloodCell, lineage (cell), cellType, c (single cell cluster), GTEx for GTEx related tissue values

	- ontologyTerms:	Organ and brain region tissues can have an EMBL-EBI Uber Anatomy Ontology identifier

	- cellosaurusID:	Cell line data has an associated cellosaurus Identifier that uniquely identifies cell lines used for the study in question.

	- additional_information:	Is additional information regarding the studied tissue which further distinct particular tissues / cells / cell lines from other, related ones

Protein_information:

	- Protein_info_ID:	Protein_information table identifier

	- Gene_ID:	Table identifier of Gene_information table

	- database:	Name of the data providing database

	- protID:	Uniprot protein ID

Protein_classes:

	- Protein_classes_ID:	Protein_classes table identifier

	- source:	Method/database which provides the protein / protein prediction

RNA_specificity:

	- RNA_Specificity_ID: RNA_specificity table ID

	- Tissue_ID: Tissue_information table identifier

	- rnaSpecificity_description:	Criteria that are matched to categorise the RNA specificity

	- rnaSpecificity_specificity:	RNA specificity category

	- rnaDistribution_description:	Semicolon separated list of descriptions of the gene expression distribution in tissues across all HPA tissues

RNA_Expression:
	- Value_ID:	RNA_Expression table identifier

	- Tissue_ID:	Tissue_information table identifier

	- Gene_ID:	Gene_information table identifier

	- Assay_ID:	Assay_information table identifier

	- normalizedRNAExpression:	Normalized RNA Expression values of the consensus Tissue data.

	- proteinCodingRNAExpression:	Protein coding RNA expression values

	- RNAExpression:	RNA expression values

	- norm-RNA-exp-unit:	Unit of the normalized RNA expression

	- prot-CodingRNA-exp_unit:	Unit of the protein coding RNA expression

	- RNA-exp_unit:	RNA expression unit

	- Text:	spaceholder at the moment

RNA_Samples:

	- RNASample_ID:	RNA_Samples table identifier

	- Tissue_ID:	Tissue_information table identifier

	- Gene_ID:	Gene_information table identifier

	- Assay_ID:	Assay_information table identifier

	- Value_ID:	RNA_Expression table identifier

	- expRNA:	Value of the sample RNA expression

	- RNA-exp_unit:	Unit of the sample RNA expression

	- sampleid:	Sample ID

	- sex:	Sample sex data

	- age:	Sample age data

scRNA_cellType:

	- scRNA_exp_ID:	scRNA_cellType table identifier

	- Tissue_ID:	Tissue_information table identifier

	- Gene_ID:	Gene_information table identifier

	- Read_count:	Read count

	- pTPM:	transcripts per million protein coding genes

GTEx_expression:

	- GTEx_exp_ID:	GTEx_expression table identifier

	- Gene_ID:	Gene_information table identifier

	- Tissue_ID:	Tissue_information table identifier

	- Expression:	normalised RNA expression values from the GTEx consensus tissues

=======
v.05
Read in the GTEx file GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_median_tpm.gct.gz and link it to the Gene_information table and Tissue_information table.

v.04
Create a helper/lookup dict to check if the combination of assay data is unique per RNA Expression.
Extract and store assembly and genecodeVersion information for each gene/entry.
Process cellTypeDistribution and add to rna_specificity_dict
Process cellTypeSpecificity and add to rna_specificty_dict
Extract scRNA celltype specific cluster information and expression, store both in a new table
Fix issue that the same tissue names appear multiple times which fox example leads to an issue in which scRNA T-Cells are the T-Cells lineage assigned to even though these should be two distinct tissues while on the other hand Adipose & soft tissue occurs multiple times. The uniq identifier sequence that is stored in the tissue_helper_dict is now all capitalized to avoid issues if in the HPA xml smaller case letters are used at individual positions. The uniq identifier sequence that is stored in the tissue_helper_dict now includes the assay  index number but the assays consensusTissue(0) and Tissue(1), humanBrain(2) and mouseBrain(5) and pigBrain(7), humanBrainRegional(3) and mouseBrainRegional(4) and pigBrainRegional(6) are treated equally respectively.

